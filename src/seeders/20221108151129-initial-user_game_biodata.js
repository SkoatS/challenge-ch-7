'use strict';



/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert(
      'user_game_biodata', [
        {
          id: 1,
          fullName: 'Bege Ganteng',
          nationality: 'Britania Raya',
          age: 17,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 2,
          fullName: 'Perdana Tampan',
          nationality: 'Selandia Baru',
          age: 24,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: 3,
          fullName: 'Bambang Perkasa',
          nationality: 'Jepang',
          age: 49,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ]
    ) 
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('user_game_biodata')
  }
};
