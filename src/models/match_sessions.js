'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class match_sessions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  match_sessions.init({
    playerOne: DataTypes.STRING,
    playerTwo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'match_sessions',
  });
  return match_sessions;
};