'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class match_room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  match_room.init({
    usergameId: DataTypes.INTEGER,
    winner: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'match_room',
  });
  return match_room;
};