'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game.hasMany(models.user_game_history, {
        foreignKey: 'id'
      })
      user_game.hasMany(models.user_game_biodata, {
        foreignKey: 'id'
      })
    }
    static register({ name, username, password}) {
      return this.create({ name, username, password: encrypt(password) });
    }

    static async authenticate({ username, password }) {
      const user = await this.findOne({ where: { username }}) // bisa di filler username menggunakan or 
      if (!user) throw new Error("User not found");

      const isPasswordValid = comparePassword(password, user.password)
      if (!isPasswordValid) throw new Error("user not found")

     return user;
    }
  }
  user_game.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};