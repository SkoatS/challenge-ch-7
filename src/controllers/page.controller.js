module.exports = {
  home: (req, res) => {
    res.render("home")
  },
  BGK: (req, res) => {
    res.render("game-bgk")
  },
  work: (req, res) => {
    res.render("work")
  },
  aboutUS: (req, res) => {
    res.render("about-us")
  },
  contactUS: (req, res) => {
    res.render("contact-us")
  },
  register: (req, res) => {
    res.render("register");
  },
  login: (req, res) => {
    console.log("data valid");
    console.log(req);
    res.render("login");
  },
  biodata: (req, res) => {
    res.render("biodata");
  },
  userGame: (req, res) => {
    res.render("user_game");
  },
  history: (req, res) => {
    res.render("history");
  }
}