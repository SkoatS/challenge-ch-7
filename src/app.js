const express = require("express");
const path = require("path");
const dotenv = require("dotenv");
const passport = require("passport");

const route = require("./routes")
const routeApi = require("./route-api")

const session  = require("express-session");
const webRoute = require("./web-route");
const logger = require("pino-http");

const pageController = require("./controllers/page.controller");



const server = (app) => {

 
  app.set("view engine", "ejs");
  app.set('views', path.join(__dirname, 'views'));

 
  
  dotenv.config()
  // middleware logging
  app.use(logger());

  app.get("/", pageController.home);
  app.get("/work", pageController.work);
  app.get("/about-us", pageController.aboutUS);
  app.get("/contact-us", pageController.contactUS);
  app.get("/game-bgk", pageController.BGK);
  app.get("/register", pageController.register);
  app.post("/register", pageController.register);
  app.get("/login", pageController.login);
  app.post("/login", pageController.login);
  app.get("/biodata", pageController.biodata);
  app.get("/history", pageController.history);
  app.get("/user_game", pageController.userGame);

  app.use(express.static("public"));

  app.use(express.static(__dirname + '/public'));
 
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

// authentication
  app.use(session({
    secret: "secret",
    resave: false,
    saveUnitialized: false
  }));
  app.use(passport.initialize());
  app.use(passport.session())

  // app.use(route);
  app.use(webRoute);
  app.use("/api", route);
  app.use("/api/v1", routeApi);

  return app;
}


module.exports = server;