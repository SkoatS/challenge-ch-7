const jwt = require("jsonwebtoken")
const { addHours, isAfter } = require("date-fns")
module.exports = {
  isAuthenticated: (req, res, next) => {
    const accesToken = req.headers.authorization;
    
    if (!accesToken) {
      return res.status(404).json({ error: "waduh! error ni." })
    }
  
    try{
      const isVerified = jwt.verify(accesToken, process.env.JWT_SECRET)
    if (!isVerified) {
      return res.status(404).json({ error: "waduh! error ni." })
    };
  
    req.session = jwt.decode(accesToken);

    // check expired token 
    const currentHour = Date.now();
    const accesTokenExpiredAt = addHours(req.session.loggedInAt, 1)
    if (isAfter(currentHour, accesTokenExpiredAt)) {
      return res.status(404).json({ error: "udah kadaluarsa ni." });
    }
  
    next();
    } catch (error) {
      return res.status(404).json({ error: "waduh! error ni." })
    } 
  },
};