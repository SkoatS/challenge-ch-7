const express = require("express");
const route = express.Router();

const { user_game } = require ("../models");


route.get("/login", async (req, res) => {
  const users = await user_game.findAll();
  return res.json(users);
});

route.put("/login/:id", async (req, res) => {
  const { body } = req;
  const put_games = await user_game.update(body, {
    where: {
      id: req.params.id
    }
  });
 return res.json(put_games);
});

route.delete("/login/:id", async (req, res) => {
  const d_games = await user_game.destroy({
    where: {
      id: req.params.id
    }
  });
 return res.json(d_games);
});

module.exports = route;