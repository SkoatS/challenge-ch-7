const express = require("express");
const route = express.Router();

const homeRoute = require("./home");
const gameRoute = require("./BGK");
const workRoute = require("./work");
const aboutUsRoute = require("./about-us");
const contactUsRoute = require("./contact-us");
const biodataRoute = require("./biodata");
const historyRoute = require("./history");
const registerRoute = require("./register");
const loginRoute = require("./login");


route.use("/", homeRoute);
route.use("/game", gameRoute);
route.use("/work", workRoute);
route.use("/about-us", aboutUsRoute);
route.use("/contact-us", contactUsRoute);
route.use("/biodata", biodataRoute);
route.use("/history", historyRoute);
route.use("/register", registerRoute);
route.use("/login", loginRoute);

module.exports = route;