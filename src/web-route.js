const express = require("express");
const webRoute = express.Router();

const { user_game, user_game_history } = require("./models");


webRoute.get("/", (req,res) => {
  res.render("home");
});

webRoute.get("/biodata", (req,res) => {
  res.render("biodata");
});

webRoute.get("/user_game", async(req, res) => {
  const data = await user_game.findAll();

  res.render("user_game", {
    data
  });
});

webRoute.get("/history", async(req, res) => {
  const data = await user_game_history.findAll();

  res.render("history", {
    data
  });
});

webRoute.get("/user_game/new", async(req,res) => {
 res.render("user_game/new", { status: 200 });
});


webRoute.post("/home", async (req, res) => {
  const { id, username, email, password } = req.body;

  // if (name != null && city != null) {
  if ( username && password ) {
    await user_game.create({ id, username, email, password });

    res.redirect("/user_game");
  } else {
    res.render("user_game/new", {
      status: 500
    });
  }
});


webRoute.get("/home/:id", async (req, res) =>{
  const data = await user_game.findOne({
    where: {
      id: req.params.id
    }
  });

  res.render("home/edit", {
    data
  });
});

webRoute.post("/home/edit/:id", async (req, res) => {
  await user_game.update({
    ...req.body,
    commission: parseFloat(req.body.commission)
  }, {
    where: {
      id: req.params.id
    }
  });

  res.redirect("/home");
});

webRoute.get("/home/delete/:id", async (req, res) => {
  await user_game.destroy({
    where: {
      id: req.params.id
    }
  });
  res.redirect("/home");
});

module.exports = webRoute;